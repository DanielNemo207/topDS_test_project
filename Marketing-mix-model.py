import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import train_test_split

# variables:
# oraganic growth rate :
# 0.01, 0.02, 0.03 have quite similar results (acceptable)
# 0.04 and 0.05 BAD
# 0.1 is the best => 0.97 score, the residual interval -10 to 10 (the best one)
# 0.11 -> 0.2 slightly worser than 0.1
# greater than 0.2 : Bad
oraganic_growth_rate = 0.1

# .775 .45 .7 seems get the best model
# score is the best,
# but the scatter of residual is reduce when we decrease the decay rate to minimum values
tv_decay = .775
rad_decay = .45
dig_decay = .75

# increase the diminishing seems make the residual more scatter
# best values are 135, 165, 85 (smallest scatter of residual and best score)
tv_dimi = 135
rad_dimi = 165
dig_dimi = 85

# read data file csv to df
simulated_data = pd.read_csv('simulated_sales.csv')
data_rows_count = simulated_data.shape[0]


# declare function to calculate adstock:
def cal_adstock(T, v, d):  # T: GPRs at that moment, v: diminishing level and b: decay from previous time
    A = (1.0 / (1.0 + np.exp(float(- T / v)))) * float(T) + float(d)
    # A = 1 - np.exp(-v / T) + d
    return float(A)


# declare function to calculate decay value:
def cal_decay_val(A, rate):  # A: the adstock value of the previous time and rate is the decay rate
    d = float(rate) * float(A)
    return float(d)


# declare fucntion to calculate adstocks of the list of GRPs
def cal_adstocks(GRPs, v, rate):
    adstocks = pd.Series(0.0, index=np.arange(data_rows_count))
    for i in range(0, len(adstocks)):
        T = GRPs[i]
        if i == 0:
            adstocks[i] = float(cal_adstock(T, v, 0))
        else:
            decay = float(cal_decay_val(adstocks[i - 1], rate))
            adstocks[i] = float(cal_adstock(T, v, decay))
    return adstocks


# declare list of grps:
tv_grps = simulated_data['tv_grps'].copy(deep=True)
radio_grps = simulated_data['radio_grps'].copy(deep=True)
digital_grps = simulated_data['digital_grps'].copy(deep=True)

# calculate adstocks
tv_adstock = cal_adstocks(tv_grps, tv_dimi, tv_decay)
rad_adstock = cal_adstocks(radio_grps, rad_dimi, rad_decay)
dig_adstock = cal_adstocks(digital_grps, dig_dimi, dig_decay)

# visualize the relationship between Adstock and GRPs
# tv ad
# tv grps bar char
tv_grps_x = range(data_rows_count)
tv_grps_y = tv_grps.values
plt.bar(tv_grps_x, tv_grps_y, 1, color="green")
# tv adstock line chart
tv_adstock_x = range(data_rows_count)
tv_adstock_y = tv_adstock
plt.plot(tv_adstock_x, tv_adstock_y)

plt.xlabel('Time')
plt.title('Tv adstock and GRPs relationship table (Green bars is GRPs and blue line is Adstock)')
plt.show()

# radio ad
# radio grps bar char
rad_grps_x = range(data_rows_count)
rad_grps_y = radio_grps.values
plt.bar(rad_grps_x, rad_grps_y, 1, color="green")
# tv adstock line chart
rad_adstock_x = range(data_rows_count)
rad_adstock_y = rad_adstock
plt.plot(rad_adstock_x, rad_adstock_y)

plt.xlabel('Time')
plt.title('Radio adstock and GRPs relationship table (Green bars is GRPs and blue line is Adstock)')
plt.show()

# Digital ad
# Digital grps bar char
dig_grps_x = range(data_rows_count)
dig_grps_y = digital_grps.values
plt.bar(dig_grps_x, dig_grps_y, 1, color="green")
# Digital adstock line chart
dig_adstock_x = range(data_rows_count)
dig_adstock_y = dig_adstock
plt.plot(dig_adstock_x, dig_adstock_y)

plt.xlabel('Time')
plt.title('Digital adstock and GRPs relationship table (Green bars is GRPs and blue line is Adstock)')
plt.show()

# preparation to build model
sales = simulated_data['sales'].copy(deep=True).values
temps = simulated_data['temp'].copy(deep=True).values

# calculate the coefficient of base value + organic growth
# basically, base value of products has coef 1 plus the growth value (%) => coef = 1 + rate * week
# I consider the base value and organic growth is the 'internal' value of the product
internal_coef = pd.Series(1.0, index=np.arange(data_rows_count))
for i in range (0, data_rows_count):
    internal_coef[i] += (i + 1) * oraganic_growth_rate

handled_data = pd.DataFrame({'sales': sales, 'internal coef': internal_coef, 'temp': temps, 'tv_adstock': tv_adstock,
                             'rad_adstock': rad_adstock, 'dig_adstock': dig_adstock})

# Split train and test sets
X = handled_data[['internal coef', 'temp', 'tv_adstock', 'rad_adstock', 'dig_adstock']]
y = handled_data[['sales']]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

# linear regression model
print ('Start training...')
regr = linear_model.LinearRegression()
model = regr.fit(X_train, y_train)
# regression theta
W = regr.coef_[0]
print('---------------------------------------------')
print ('The training result: ', W)
print ('Base value: ', W[0])
print ('Temp Coef: ', W[1])
print ('Tv adstock Coef: ', W[2])
print ('Rad adstock Coef: ', W[3])
print ('Dig adstock Coef: ', W[4])

# Evaluate the model
y_prediction = regr.predict(X_test)
y_true = y_test.values

# Scatter result
min = min(min(y_true), min(y_prediction))[0]
max = max(max(y_true), max(y_prediction))[0]
line_x = np.arange(min, max)
line_y = line_x
plt.plot(line_x, line_y, "-", color='green')

plt.scatter(y_test, y_prediction)
plt.xlabel('True Values, y')
plt.ylabel('Predictions, y_hat')
plt.show()

# Line plot
plt.plot(np.arange(0, len(y_true)), y_true, "-", color='green')
plt.plot(np.arange(0, len(y_true)), y_prediction, "-", color='red')
plt.show()

# Residual plot
difference = y_prediction - y_true
plt.plot(np.arange(0, len(y_true)), difference, 'or')
plt.show()

# Score
print('---------------------------------------------')
print('Score of model: ', model.score(X_test, y_test))

# Estimate boosted sales by advitising activities
estimated_sales_by_tv = 0
estimated_sales_by_rad = 0
estimated_sales_by_dig = 0
total_sales = 0

for e in range (0, data_rows_count):
    estimated_sales_by_tv += W[2] * handled_data.iloc[e]['tv_adstock']
    estimated_sales_by_rad += W[3] * handled_data.iloc[e]['rad_adstock']
    estimated_sales_by_dig += W[4] * handled_data.iloc[e]['dig_adstock']
    total_sales += handled_data.iloc[e]['sales']

print('---------------------------------------------')
print ('The precentage of sales boosted by tv ad: ', estimated_sales_by_tv * 100 / total_sales)
print ('The precentage of sales boosted by radio ad: ', estimated_sales_by_rad * 100 / total_sales)
print ('The precentage of sales boosted by digital ad: ', estimated_sales_by_dig * 100 / total_sales)
# The boosted percentage of sales are seemly not realistic :(

print('---------------------------------------------')
print('Finished ...')